package com.afaray.poker;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import com.afaray.poker.entity.Jugador;
import com.afaray.poker.entity.util.Constant;

public class PokerApp {
	public static void main(String[] args) {
		ArrayList<Jugador> jugadores = new ArrayList<>();
		Juego jugar = null;
		Scanner scn = new Scanner(System.in);
		int opc = -1, cantidadJugadores = 0;
//		String validarJuegoConComodines;
		boolean conComodines = false;

		//TODO: Falta agregar validacion de comodines
//		System.out.println("Antes de empezar, desea jugar con comodines? (S/N)");
//		validarJuegoConComodines = scn.nextLine();
//		if ("S".equalsIgnoreCase(validarJuegoConComodines))
//			conComodines = true;

		do {
			try {
				printMenu();
				opc = Integer.parseInt(scn.nextLine());
				switch (opc) {
				case 1:
					if (cantidadJugadores <= maxJugadores(conComodines)) {
						String nombreJ;
						System.out.println("Agregue a nuevo jugador. \nIngrese nombre");
						nombreJ = scn.nextLine();
						//TODO agregar valores para apuestas
//						int dinero = 0;
//						System.out.println("Agregue dinero disponible para jugar");
//						try {
//							dinero = Integer.parseInt(scn.nextLine());
//						} catch (NumberFormatException e) {
//							System.out.println("Monto ingresado no es valido, no se ingresar� usuario");
//							break;
//						}

						Jugador newj = new Jugador(nombreJ);
						jugadores.add(newj);
						cantidadJugadores++;
					} else {
						System.out.println("No se puede agregar m�s jugadores");
					}
					break;
				case 2:
					if (cantidadJugadores > 1) {
						System.out.println("Se comienza la partida con los jugadores: ");
						jugadores.stream().map(Jugador::getNombreJugador).forEach(System.out::println);

						if (null == jugar) 
							jugar = new Juego(conComodines, cantidadJugadores, jugadores);
						
						// jugar.getBaraja().getMazo().stream().map(Carta::getCarta).forEach(System.out::println);
						System.out.println("Se reparten cartas\n");
						jugadores.stream().forEach(jugar::reparteCartas);

						System.out.println("Determinar ganador\n");
						for (Iterator<Jugador> iterator = jugadores.iterator(); iterator.hasNext();) {
							Jugador jugador = (Jugador) iterator.next();
							jugador.verMano();
							System.out.println("\n\n");
						}
						
						Jugador winner = jugar.elegirGanador(jugadores);
						System.out.println("El ganador es: " + winner.getNombreJugador() + "!!!");
					} else {
						System.out.println("No existen jugadores ingresados\n");
					}

					break;
				case 0:
					System.out.println("Adios");
					scn.close();
					break;
				default:
					System.out.println("Seleccione opcion valida");
					opc = Integer.parseInt(scn.nextLine());
					break;
				}
			} catch (NumberFormatException e) {
				System.out.println("Valor ingresado no es un numero");
				printMenu();
			}
		} while (opc != 0);
	}

	static int maxJugadores(boolean como) {
		if (como)
			return (int) Math.floor(Constant.BARAJA_CANT_CARTAS + 2 / Constant.CARTAS_A_REPARTIR);
		else
			return (int) Math.floor(Constant.BARAJA_CANT_CARTAS / Constant.CARTAS_A_REPARTIR);
	}
	
	static void printMenu(){
		System.out.println("Seleccione una opción");
		System.out.println("1 .- Ingresar Jugadores");
		System.out.println("2 .- Iniciar Juego");
		System.out.println("0 .- Salir");
	}
}
