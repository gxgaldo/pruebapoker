package com.afaray.poker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.afaray.poker.entity.Baraja;
import com.afaray.poker.entity.Carta;
import com.afaray.poker.entity.Jugador;
import com.afaray.poker.entity.util.Constant;
import com.afaray.poker.entity.util.JuegoEnum;

public class Juego {
	private int numeroJugadores;
	private boolean juegoConComodines;

	private Baraja baraja;
	private ArrayList<Jugador> jugadores;

	public Juego(boolean comod, int jug, ArrayList<Jugador> jugadores) {
		this.juegoConComodines = comod;
		this.numeroJugadores = jug;
		this.jugadores = jugadores;
		initJuego();
	}

	public void initJuego() {
		this.baraja = new Baraja(this.juegoConComodines);
	}

	public void reparteCartas(Jugador jug) {
		ArrayList<Carta> mano = new ArrayList<>();
		for (int i = 1; i <= Constant.CARTAS_A_REPARTIR; i++) {
			Carta cart = this.baraja.robarDeMazo();
			if (null != cart)
				mano.add(cart);
		}

		jug.setMano(mano);
	}

	public void retirarJugador(Jugador jug) {
		System.out.println("Jugador " + jug.getNombreJugador() + "se retira");
		jugadores.remove(jug);
	}

	public Jugador elegirGanador(ArrayList<Jugador> jgdrs) {
		Map<Jugador, Object> resultados = new HashMap<Jugador, Object>();
		Jugador jug = null;

		for (Iterator<Jugador> iterator = jgdrs.iterator(); iterator.hasNext();) {
			Jugador jugador = (Jugador) iterator.next();
			// System.out.println("==============================================");
			// System.out.println("======" + jugador.getNombreJugador() +
			// "======");
			resultados.put(jugador, validarMano(jugador.getMano()));
		}

		return evalResultados(resultados);
	}

	public Map<String, String> validarMano(ArrayList<Carta> mano) {
		// genero contador de repeticiones
		// para revisar pares y trios
		
		// Inicializo arrays en 5, dado la cartas en juego
		Integer maxCount = 0, secondMaxCount = 0, contadorIt = 0;
		String nombreCarta = "", nombreCartaSec = "";
		String[] todoColor = new String[5];
		boolean inSecuence = false, allColorFlush = false;
		int[] validadorNumero = new int[5];

		// Formateo la data y la guardo en un map
		Map<Integer, String[]> contenedor = new HashMap<Integer, String[]>();
		for (Carta c : mano) {
			Integer numCarta = c.getNumCarta();
			String colCarta = c.getColor().getColor();
			String nomCarta = c.getCarta();

			if (null == contenedor.get(numCarta))
				contenedor.put(numCarta, new String[] { nomCarta, colCarta, "1" });
			else {
				String colores = contenedor.get(numCarta)[1] + "-" + colCarta;
				String cont = String.valueOf(Integer.parseInt(contenedor.get(numCarta)[2]) + 1);
				contenedor.put(numCarta, new String[] { nomCarta, colores, cont });
			}
		}

		
		for (Map.Entry<Integer, String[]> entrada : contenedor.entrySet()) {
			Integer key = entrada.getKey();
			String[] vals = entrada.getValue();

			Integer carta = key;
			Integer value = Integer.parseInt(vals[2]);

			todoColor[contadorIt] = vals[1];
			validadorNumero[contadorIt] = carta;

			if (value > maxCount) {
				nombreCarta = vals[0];
				maxCount = value;
			} else if ((value > secondMaxCount && value < maxCount) || value == maxCount) {
				nombreCartaSec = vals[0];
				secondMaxCount = value;
			}

			contadorIt++;

		}

		inSecuence = validateSecuence(validadorNumero);
		allColorFlush = evaluateAllColor(todoColor);

		Map<String, String> result = new HashMap<String, String>();

		//TODO: Falta agregar validacion de comodines
		// Validacion con los datos ya preparados
		if (inSecuence && allColorFlush) { // StraighFlush 5 cartas en secuencia
											// del mismo color
			result.put(Constant.RESULT, String.valueOf(JuegoEnum.STRAIGHFLUSH.getVal()));
		} else if (maxCount == 4) { // FourdOfAKind 4 cartas iguales
			result.put(Constant.RESULT, String.valueOf(JuegoEnum.FOROFAKIND.getVal()));
		} else if (maxCount == 3 && secondMaxCount == 2) { // FullHouse un trio
															// y una pareja
			result.put(Constant.RESULT, String.valueOf(JuegoEnum.FULLHOUSE.getVal()));
		} else if (inSecuence) { // Straight 5 cartas en secuencia
			result.put(Constant.RESULT, String.valueOf(JuegoEnum.STRAIGH.getVal()));
		} else if (maxCount == 3) { // ThreeOfAKind un trio.
			result.put(Constant.RESULT, String.valueOf(JuegoEnum.THREEOFAKIND.getVal()));
		} else if (maxCount == 2 && secondMaxCount == 2) { // TwoPair dos pares
			result.put(Constant.RESULT, String.valueOf(JuegoEnum.TWOPAIR.getVal()));
		} else if (maxCount == 2 && secondMaxCount == 1) { // OnePair un par
			result.put(Constant.RESULT, String.valueOf(JuegoEnum.ONEPAIR.getVal()));
		} else { // HighCard carta más alta
			result.put(Constant.RESULT, String.valueOf(JuegoEnum.HIGHCARD.getVal()));
			result.put(Constant.HIGHCARD, String.valueOf(Arrays.stream(validadorNumero).max().getAsInt()));
		}

		result.put(Constant.CARTA, nombreCarta);
		result.put(Constant.CARTA_SEC, nombreCartaSec);

		// contenedor.forEach((k, v) -> {
		// System.out.println("Carta : " + v[0]);
		// System.out.println("Color (es) : " + v[1]);
		// System.out.println("Contador : " + v[2]);
		// System.out.println("======================");
		// });

		return result;
	}

	private boolean validateSecuence(int[] sort) {
		Integer nextNumber = 0;
		for (int i = 0; i < sort.length; i++) {
			if (i == 0) {
				nextNumber = sort[i] + 1;
				continue;
			}

			if (nextNumber != sort[i + 1])
				return false;
			else
				nextNumber = sort[i] + 1;
		}

		return true;

	}

	private boolean evaluateAllColor(String[] color) {
		String lastColor = "";

		for (int i = 0; i < color.length; i++) {
			if (0 == i) {
				lastColor = color[i];
				continue;
			}

			if ((color[i].contains("-")) || (lastColor != color[i]))
				return false;
			else
				lastColor = color[i];
		}

		return true;
	}

	private Jugador evalResultados(Map<Jugador, Object> resultados) {
		Jugador lastJ = null;
		String lastJHC = "", lastJCard = "", lastJCardS = "", lastResult = "";
		int cont = 0;
		
		for (Entry<Jugador, Object> entrada : resultados.entrySet()) {
			Jugador key = entrada.getKey();
			Map<?, ?> values = (Map<?, ?>) entrada.getValue();
			
			if(cont == 0){
				lastJ = key;
				lastResult = (String) values.get("result");
				lastJHC =  (String) values.get("highCard");
				lastJCard =  (String) values.get("carta");
				lastJCardS =  (String) values.get("carta2");
			} else {
				if(Integer.parseInt(lastResult) > Integer.parseInt((String) values.get("result"))){ //enum hacia abajo
					lastJ = key;
					lastResult = (String) values.get("result");
					lastJHC =  (String) values.get("highCard");
					lastJCard =  (String) values.get("carta");
					lastJCardS =  (String) values.get("carta2");
				} else if (Integer.parseInt(lastResult) == Integer.parseInt((String) values.get("result"))){ //resultado igual de enum
					//validaciones de cartas
					boolean cambiarDatos = false;
					
					if(("0".equals(lastResult) || "4".equals(lastResult) || "8".equals(lastResult))
							&& Integer.parseInt(lastJHC) < Integer.parseInt((String)values.get("highCard"))){
						cambiarDatos = true; //carta más alta de la secuencia
					} else if(("1".equals(lastResult) || "5".equals(lastResult) || "7".equals(lastResult))
							&& (Integer.parseInt(lastJHC) < Integer.parseInt((String)values.get("highCard")))){
						cambiarDatos = true;  //un solo maximo
					} else if (("2".equals(lastResult) || "6".equals(lastResult))
							&& ((Integer.parseInt(lastJCard) < Integer.parseInt((String)values.get("carta"))) || 
							((Integer.parseInt(lastJCardS) < Integer.parseInt((String)values.get("carta2")))))){
						cambiarDatos = true;  //dos maximos
					} 						
					
					//TODO: Falta validación 3 que es por colores de cartas de la misma pinta
					
					
					if(cambiarDatos){
						lastJ = key;
						lastResult = (String) values.get("result");
						lastJHC =  (String) values.get("highCard");
						lastJCard =  (String) values.get("carta");
						lastJCardS =  (String) values.get("carta2");
					}
					
				}
			}
			cont++;
		}
		
		return lastJ;

	}

	/**
	 * @return the numeroJugadores
	 */
	public int getNumeroJugadores() {
		return numeroJugadores;
	}

	/**
	 * @param numeroJugadores
	 *            the numeroJugadores to set
	 */
	public void setNumeroJugadores(int numeroJugadores) {
		this.numeroJugadores = numeroJugadores;
	}

	/**
	 * @return the juegoConComodines
	 */
	public boolean isJuegoConComodines() {
		return juegoConComodines;
	}

	/**
	 * @param juegoConComodines
	 *            the juegoConComodines to set
	 */
	public void setJuegoConComodines(boolean juegoConComodines) {
		this.juegoConComodines = juegoConComodines;
	}

	/**
	 * @return the baraja
	 */
	public Baraja getBaraja() {
		return baraja;
	}

	/**
	 * @param baraja
	 *            the baraja to set
	 */
	public void setBaraja(Baraja baraja) {
		this.baraja = baraja;
	}

	/**
	 * @return the jugadores
	 */
	public ArrayList<Jugador> getJugadores() {
		return jugadores;
	}

	/**
	 * @param jugadores
	 *            the jugadores to set
	 */
	public void setJugadores(ArrayList<Jugador> jugadores) {
		this.jugadores = jugadores;
	}

}
