package com.afaray.poker.entity;

import java.util.ArrayList;

public class Jugador {
	private String nombreJugador;
	private ArrayList<Carta> mano;
	
	public Jugador(String nom){
		this.nombreJugador = nom;
	}
	
	public Jugador(String nom, ArrayList<Carta> cartas) {
		this.nombreJugador = nom;
		this.mano = cartas;
	}

	public void verMano(){
		System.out.println("Las cartas del jugador: " + this.nombreJugador + ":");
		
		if(null != this.mano && this.mano.size()>0)
			this.mano
				.stream()
				.map(Carta::toString)
				.forEach(System.out::println);
		else 
			System.out.println("La mano esta vacia");
	}
	
	/**
	 * @return the nombreJugador
	 */
	public String getNombreJugador() {
		return nombreJugador;
	}


	/**
	 * @param nombreJugador the nombreJugador to set
	 */
	public void setNombreJugador(String nombreJugador) {
		this.nombreJugador = nombreJugador;
	}


	/**
	 * @return the mano
	 */
	public ArrayList<Carta> getMano() {
		return mano;
	}


	/**
	 * @param mano the mano to set
	 */
	public void setMano(ArrayList<Carta> mano) {
		this.mano = mano;
	}

	
	public String toString(){
		return "Nombre de jugador: "+ this.nombreJugador + "\n ";
	}

}
