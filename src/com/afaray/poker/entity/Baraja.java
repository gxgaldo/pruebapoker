package com.afaray.poker.entity;

import java.util.ArrayList;
import java.util.Collections;

import com.afaray.poker.entity.util.Constant;

public class Baraja {
	private ArrayList<Carta> mazo;
	private boolean llevaComondines;

	public Baraja(boolean comodin) {
		this.llevaComondines = comodin;
		initBaraja();
	}

	/**
	 * inicializa baraja y la ordena aleatoriamente
	 */
	public void initBaraja() {
		this.mazo = new ArrayList<>();
		ArrayList<Carta> colorPica = new ArrayList<>();
		ArrayList<Carta> colorCorazon = new ArrayList<>();
		ArrayList<Carta> colorTrebol = new ArrayList<>();
		ArrayList<Carta> colorDiamante = new ArrayList<>();

		colorPica = generaColor(Constant.PICA);
		colorCorazon = generaColor(Constant.CORAZON);
		colorTrebol = generaColor(Constant.TREBOL);
		colorDiamante = generaColor(Constant.DIAMANTE);

		this.mazo.addAll(colorPica);
		this.mazo.addAll(colorCorazon);
		this.mazo.addAll(colorTrebol);
		this.mazo.addAll(colorDiamante);

		if (this.llevaComondines) {
			Carta comodinRojo = new Carta(0, new ColorCarta(Constant.ROJO),true);
			Carta comodinNegro = new Carta(0, new ColorCarta(Constant.NEGRO),true);
			this.mazo.add(comodinNegro);
			this.mazo.add(comodinRojo);
		}

		Collections.shuffle(getMazo());
	}

	public Carta robarDeMazo() {
		if(!getMazo().isEmpty()){
			Carta sustraer = getMazo().get(0);
			getMazo().remove(0);
			return sustraer;
		} else return null;
		
		
	}

	private ArrayList<Carta> generaColor(String color) {
		ArrayList<Carta> generador = new ArrayList<>();
		ColorCarta col = new ColorCarta(color);

		for (int i = 1; i < 13; i++) {
			Carta carta = new Carta(i, col);
			generador.add(carta);
		}

		return generador;
	}

	/**
	 * @return the llevaComondines
	 */
	public boolean isLlevaComondines() {
		return llevaComondines;
	}

	/**
	 * @param llevaComondines
	 *            the llevaComondines to set
	 */
	public void setLlevaComondines(boolean llevaComondines) {
		this.llevaComondines = llevaComondines;
	}

	/**
	 * @return the mazo
	 */
	public ArrayList<Carta> getMazo() {
		return mazo;
	}

	/**
	 * @param mazo
	 *            the mazo to set
	 */
	public void setMazo(ArrayList<Carta> mazo) {
		this.mazo = mazo;
	}

}
