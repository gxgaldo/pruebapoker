package com.afaray.poker.entity.util;

public final class Constant {
	public static final int BARAJA_CANT_CARTAS = 52;
	public static final int CARTAS_A_REPARTIR = 5;
	public static final String NEGRO = "Negro";
	public static final String ROJO = "Rojo";
	
	public static final String PICA = "Pica";
	public static final String CORAZON = "Corazón";
	public static final String TREBOL = "Trébol";
	public static final String DIAMANTE = "Diamante";
	
	public static final String RESULT = "result";
	public static final String HIGHCARD = "highCard";
	public static final String CARTA = "carta";
	public static final String CARTA_SEC = "carta2";
	
}
