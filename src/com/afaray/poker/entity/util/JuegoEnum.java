package com.afaray.poker.entity.util;

public enum JuegoEnum {
	//StraighFlush 5 cartas en secuencia del mismo color
	STRAIGHFLUSH(0), //
	
	//FourdOfAKind 4 cartas iguales
	FOROFAKIND(1), //
	
	//FullHouse un trio y una pareja
	FULLHOUSE(2), //
	
	//Flush 5 cartas mismo color
	FLUSH(3), //
	
	//Straight 5 cartas en secuencia
	STRAIGH(4),
	
	//ThreeOfAKind un trio.
	THREEOFAKIND(5),
	
	//TwoPair dos pares
	TWOPAIR(6), 
	
	//OnePair un par
	ONEPAIR(7), //
	
	//HighCard carta más alta
	HIGHCARD(8);
	
	private int val;
	
	private JuegoEnum(int value) {
		this.val = value;
	}

	/**
	 * @return the val
	 */
	public int getVal() {
		return val;
	}

	/**
	 * @param val the val to set
	 */
	public void setVal(int val) {
		this.val = val;
	}
}
