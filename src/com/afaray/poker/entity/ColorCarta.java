package com.afaray.poker.entity;

public class ColorCarta {

	private String color;
	private boolean esComodin;
	
	public ColorCarta(String color){
		this.color = color;
	}
	
	public ColorCarta(String color, boolean comod) {
		this.color = color;
		this.esComodin = comod;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the esComodin
	 */
	public boolean esComodin() {
		return esComodin;
	}

	/**
	 * @param esComodin the esComodin to set
	 */
	public void setEsComodin(boolean esComodin) {
		this.esComodin = esComodin;
	}
	
}