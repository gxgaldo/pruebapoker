package com.afaray.poker.entity;

public class Carta {

	private int numCarta;
	private ColorCarta color;
	private boolean esComodin;

	public Carta(int num, ColorCarta col) {
		this.numCarta = num;
		this.color = col;
	}
	
	public Carta(int num, ColorCarta col, boolean como) {
		this.numCarta = num;
		this.color = col;
		this.esComodin = como;
	}

	public String getCarta() {
		String car = null;
		String val = String.valueOf(getNumCarta());

		switch (val) {
			case "1":
				car = "A";
				break;
			case "11":
				car = "J";
				break;
			case "12":
				car = "Q";
				break;
			case "13":
				car = "K";
				break;
			case "0":
				car = "Comodin";
				break;
			default:
				car = val;
				break;
		}

		return car;
	}

	/**
	 * @return the numCarta
	 */
	public int getNumCarta() {
		return numCarta;
	}

	/**
	 * @param numCarta
	 *            the numCarta to set
	 */
	public void setNumCarta(int numCarta) {
		this.numCarta = numCarta;
	}

	/**
	 * @return the color
	 */
	public ColorCarta getColor() {
		return color;
	}

	/**
	 * @param color
	 *            the color to set
	 */
	public void setColor(ColorCarta color) {
		this.color = color;
	}

	/**
	 * @return the esComodin
	 */
	public boolean isEsComodin() {
		return esComodin;
	}

	/**
	 * @param esComodin the esComodin to set
	 */
	public void setEsComodin(boolean esComodin) {
		this.esComodin = esComodin;
	}
	
	public String toString(){
		return "Carta : " +this.getCarta()+ ", Color: "+ this.color.getColor() + ", esComodin: " + ((this.esComodin)?"Si":"No");
	}

}
